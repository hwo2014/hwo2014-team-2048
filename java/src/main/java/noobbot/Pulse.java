package noobbot;

import java.util.SortedMap;
import java.util.TreeMap;

import static java.lang.Math.abs;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

/**
 * @author Sergii Ostroverkhov
 * @since 20/04/2014
 */
public class Pulse {
    static final double MAX_THROTTLE = 1.0;
    static final double MIN_THROTTLE = 0.0;

    static final double CRASH_SPEED = 7.055155687180871;
    static final int CRASH_RADIUS = 110;
    static final double ANGLE_SPEED = CRASH_SPEED * CRASH_SPEED / CRASH_RADIUS;
    static final double F = CRASH_SPEED * CRASH_SPEED / CRASH_RADIUS * sin(toRadians(55));

    BrakeStrategy brakeStrategy;
    TurboStrategy turboStrategy;

    Motion motion = null;

    Race race = null;

    SortedMap<Integer, Tick> tickMap = new TreeMap<>();

    public Pulse() {
        System.out.print("tick");
        System.out.print("\tlap");
        System.out.print("\tpiece");
        System.out.print("\tlength");
        System.out.print("\tradius");
        System.out.print("\tdistance");
        System.out.print("\tspeed");
        System.out.print("\tthrottle");
        System.out.print("\tangle");
        System.out.print("\tangleSpeed");
        System.out.println();
    }

    public double throttle(final Integer tickNumber, final CarPosition position) {
        Double throttle;

        Tick tick = new Tick(tickNumber, position);
        Tick tickPrev = tickMap.get(tickNumber - 1);
        Race.Track.Piece currentPiece = race.track.pieces[position.piecePosition.pieceIndex];
        Race.Track.Piece nextPiece = race.getPieceAfter(position.piecePosition.pieceIndex);

        if (tickPrev != null) {
            calculateSpeed(tick, tickPrev);
            if (turboStrategy != null) {
                turboStrategy.plan(currentPiece, nextPiece);
            }

            tick.carAngleSpeed = abs(tick.carPosition.angle - tickPrev.carPosition.angle);

            if (tickMap.size() > 2) {
                /* //Duplication for parabola strategy for better result, will be optimized after all length will be available
                Tick tick1 = tickMap.get(tick.number - 2);
                Tick tick2 = tickMap.get(tick.number - 1);
                Tick tick3 = tick;
                if (tick1.throttle == tick2.throttle && tick1.speed <= tick2.speed
                        && tick1.distance() <= tick2.distance() && tick2.distance() <= tick3.distance()) {
                    motion = new Motion(tick1.distance(), tick2.distance(), tick3.distance(), MAX_THROTTLE); // Should be tick2.throttle
                } // Duplication for parabola strategy */

                Turn nextTurn = Turn.getNextTurn(position.piecePosition.pieceIndex, race.track.pieces,
                        position.piecePosition.inPieceDistance, getDistanceFromCenter(position));
                Turn nextNextTurn = Turn.getNextNextTurn(nextTurn, race.track.pieces);

                double currentF = abs(tick.speed * tick.speed / nextTurn.piece.radius(getDistanceFromCenter(position)) * sin(toRadians(position.angle)));
                Double possibleF;
                try {
                    possibleF = abs(tick.speed * tick.speed / nextNextTurn.piece.radius(getDistanceFromCenter(position)) * sin(toRadians(position.angle)));
                } catch (NullPointerException e) {
                    possibleF = null;
                }
                if (currentF > F || possibleF != null && possibleF > F) {
                    System.out.println(currentF + ">>>>>>>>>>>>>>>>>>>" + F);
                    throttle = MIN_THROTTLE;
                } else
                if (brakeStrategy != null) {
                    throttle = brakeStrategy.getThrottle(position.piecePosition.pieceIndex, tick.speed,
                            position.piecePosition.inPieceDistance, getPiece(position).length(getDistanceFromCenter(position)));
                    if (throttle == null) {
                        brakeStrategy = null;
                        throttle = tickPrev.throttle;
                    }
                } else {
                    Race.Track.Piece piece = nextTurn.piece;
                    if (piece.radius != null) {
                        double maxSpeed = sqrt(ANGLE_SPEED * piece.radius(getDistanceFromCenter(position)));

                        if (beforeTurnStrategy(tick, nextTurn, maxSpeed)) {
                            throttle = maxSpeed / 10;
                            if (throttle > MAX_THROTTLE) {
                                throttle = MAX_THROTTLE;
                            } else {
                                BrakeStrategy brakeStrategy = new BrakeStrategy();
                                brakeStrategy.throttleAfterBrake = throttle;
                                brakeStrategy.brakeToSpeed = maxSpeed;
                                brakeStrategy.fromIndex = position.piecePosition.pieceIndex;
                                brakeStrategy.toIndex = nextTurn.index;
                                if (tick.speed > maxSpeed) {
                                    throttle = MIN_THROTTLE;
                                }
                                this.brakeStrategy = brakeStrategy;
                            }
                        } else {
                            throttle = MAX_THROTTLE;
                        }
                    } else {
                        throttle = tickPrev.throttle;
                    }
                }
            } else {
                throttle = MAX_THROTTLE;
            }

            tick.throttle = throttle;
        }

        System.out.printf("%d", tickNumber);
        System.out.printf("\t%d", position.piecePosition.lap);
        System.out.printf("\t%d", position.piecePosition.pieceIndex);
        System.out.printf("\t%.1f", getPiece(position).length(getDistanceFromCenter(position)));
        System.out.printf("\t%d", getPiece(position).radius(getDistanceFromCenter(position)));
        System.out.printf("\t%.4f", position.piecePosition.inPieceDistance);
        System.out.printf("\t%.4f", tick.speed);
        System.out.printf("\t%.4f", tick.throttle);
        System.out.printf("\t%.4f", position.angle);
        System.out.printf("\t%.4f", tick.carAngleSpeed);
        System.out.printf("\t%.4f", tick.speed * tick.speed / getPiece(position).radius(getDistanceFromCenter(position)) * sin(toRadians(position.angle)));
        System.out.printf("\t%.4f", sqrt(F * getPiece(position).radius(getDistanceFromCenter(position)) / sin(toRadians(position.angle))));
        System.out.println();

        tickMap.put(tickNumber, tick);
        return tick.throttle;
    }

    private Race.Track.Piece getPiece(final CarPosition position) {
        return race.track.pieces[position.piecePosition.pieceIndex];
    }

    private Integer getDistanceFromCenter(final CarPosition position) {
        return race.track.lanes[position.piecePosition.lane.endLaneIndex].distanceFromCenter;
    }

    private void calculateSpeed(final Tick tick, final Tick tickPrev) {
        Double distance1 = tickPrev.distance();
        Double distance2 = tick.distance();
        if (distance1 > distance2) {
            Integer pieceIndex = tickPrev.carPosition.piecePosition.pieceIndex;
            Race.Track.Piece prevPiece = race.track.pieces[pieceIndex];
            Integer laneIndex = tickPrev.carPosition.piecePosition.lane.endLaneIndex;
            Race.Track.Lane lane = race.track.lanes[laneIndex];
            distance2 += prevPiece.length(lane.distanceFromCenter);
        }
        double speed = getSpeed(distance1, distance2);
        tick.speed = speed;
        tick.acceleration = speed - tickPrev.speed;
    }

    private boolean beforeTurnStrategy(final Tick tick, final Turn nextTurn, final double maxSpeed) {
//        return parabolaStrategy(tick, nextTurn, maxSpeed);
        return dumpStrategy(tick, nextTurn, maxSpeed);
//        return lineStrategy(tick, nextTurn, maxSpeed);
    }

    private boolean dumpStrategy(final Tick tick, final Turn nextTurn, final double maxSpeed) {
        double marginDistance = Dump.brakeDistance(tick.speed, maxSpeed);
//        System.out.println("marginDistance > nextTurn.distance;" + marginDistance + ">" + nextTurn.distance);
        return tick.speed > maxSpeed && marginDistance >= nextTurn.distance;
    }

    private boolean parabolaStrategy(final Tick tick, final Turn nextTurn, final double maxSpeed) {
        Tick tick1 = tickMap.get(tick.number - 2);
        Tick tick2 = tickMap.get(tick.number - 1);
        Tick tick3 = tick;
        if (tick1.throttle == tick2.throttle && tick1.speed <= tick2.speed
                && tick1.distance() <= tick2.distance() && tick2.distance() <= tick3.distance()) {
            motion = new Motion(tick1.distance(), tick2.distance(), tick3.distance(), MAX_THROTTLE); // Should be tick2.throttle
        }

        double ratio1 = motion.getRatio(4);
        double ratio2 = motion.getRatio(5);
        double possibleDistance1 = tick2.throttle * ratio1;
        double possibleDistance2 = tick2.throttle * ratio2;
        double possibleSpeed = getSpeed(possibleDistance1, possibleDistance2);
        double possibleDistanceToTurn = possibleDistance1 - tick3.distance();

        return nextTurn.distance < possibleDistanceToTurn && possibleSpeed > maxSpeed;
    }

    private boolean lineStrategy(final Tick tick, final Turn nextTurn, final double maxSpeed) {
        Tick tick0 = tickMap.get(tick.number - 1);
        double k = tick.distance() - tick0.distance();
        double b = tick0.distance();
        double possibleDistance1 = k*14+b;
        double possibleDistance2 = k*15+b;
        double possibleSpeed = getSpeed(possibleDistance1, possibleDistance2);
        double possibleDistanceToTurn = possibleDistance1 - tick.distance();

        return nextTurn.distance < possibleDistanceToTurn && possibleSpeed > maxSpeed;
    }

    private double getSpeed(final Double distance1, final Double distance2) {
        return sqrt((distance2 - distance1) * (distance2 - distance1));
    }

    public void calculateCurvesLength() {
        for (Race.Track.Piece piece : race.track.pieces) {
            if (piece.radius != null) {
                //TODO Fix for specific lane radius
                piece.length = abs(Math.PI * (piece.radius) * piece.angle / 180);
            }
        }
    }

    public void flagCurvesPositions() {
        Race.Track.Piece[] pieces = race.track.pieces;
        int i = 0;
        while (i < pieces.length) {
            if (pieces[i].radius != null) {
                pieces[i].turnStart = true;
                int j = i + 1;
                while (j < pieces.length && pieces[j].radius != null) {
                    j++;
                }
                pieces[j - 1].turnEnd = true;
                i = j - 1;
            }
            i++;
        }

    }

    public void flagLines() {
        Race.Track.Piece[] pieces = race.track.pieces;
        int i = 0;
        while (i < pieces.length) {
            if (pieces[i].radius == null) {
                pieces[i].lineStart = true;
                int lineStartPosition = i;
                int j = i + 1;
                Double lineLength = pieces[i].length;
                while (j < pieces.length && pieces[j].radius == null) {
                    lineLength += pieces[j].length;
                    j++;
                }
                i = j - 1;
                if (j == pieces.length) {
                    j = 0;
                    while (j < pieces.length && pieces[j].radius == null) {
                        lineLength += pieces[j].length;
                        j++;
                    }
                }
                pieces[j - 1].lineEnd = true;
                pieces[lineStartPosition].lineLength = lineLength;
            }
            i++;
        }

    }
}
