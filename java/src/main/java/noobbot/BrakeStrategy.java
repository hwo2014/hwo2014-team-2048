package noobbot;

/**
 * @author Sergii Ostroverkhov
 * @since 20/04/2014
 */
public class BrakeStrategy {
    double brakeToSpeed;
    double throttleAfterBrake;
    int fromIndex;
    int toIndex;

    public Double getThrottle(final int pieceIndex, final double speed, final Double inPieceDistance, final Double length) {
        Double throttle;
        if (isActual(pieceIndex)
                || (pieceIndex == toIndex && inPieceDistance < length / 2.0)) {
            if (speed > brakeToSpeed) {
                throttle = Pulse.MIN_THROTTLE;
            } else {
                throttle = throttleAfterBrake;
            }
        } else {
            throttle = null;
        }
        return throttle;
    }

    private boolean isActual(final int pieceIndex) {
        return fromIndex < toIndex && pieceIndex < toIndex
                || fromIndex > toIndex && (pieceIndex > fromIndex || pieceIndex < toIndex);
    }
}
