package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        SendMsg initMsg;
        if (args.length > 4) {
            String trackName = args[4];
            initMsg = new CreateRace(botName, botKey, trackName, 1, null);
        } else {
            initMsg = new Join(botName, botKey);
        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, initMsg);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    Pulse pulse = new Pulse();

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg initMsg) throws IOException {
        this.writer = writer;
        String line;

        send(initMsg);

        while((line = reader.readLine()) != null) {
            //System.out.println(line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            switch (msgFromServer.msgType) {
                case "carPositions": {
                    List cars = (List) msgFromServer.data;
                    JsonObject element = gson.toJsonTree(cars.get(0)).getAsJsonObject();
                    Integer tickNumber = msgFromServer.gameTick == null ? 0 : msgFromServer.gameTick;
                    CarPosition position = gson.fromJson(element, CarPosition.class);
                    double throttle = pulse.throttle(tickNumber, position);
                    if (pulse.turboStrategy != null && pulse.turboStrategy.ready()) {
                        if (!pulse.turboStrategy.activated()) {
                            send(new Turbo());
//                            if (!pulse.turboStrategy.activated()
//                                    && position.piecePosition.inPieceDistance >=
//                                    pulse.race.track.pieces[position.piecePosition.pieceIndex].length / 1.5) {
//                                send(new Throttle(0.5));

                            pulse.turboStrategy.activate();
                        } else {
                            send(new Ping());
                        }
                        pulse.turboStrategy.tick();
                        if (pulse.turboStrategy.finished()) {
                            pulse.turboStrategy = null;
                        }
                    } else {
                        send(new Throttle(throttle));
                    }
                    break;
                }
                case "join":
                    System.out.println("Joined");
                    break;
                case "gameInit": {
                    JsonObject element = gson.toJsonTree(msgFromServer.data).getAsJsonObject();
                    pulse.race = gson.fromJson(element.getAsJsonObject("race"), Race.class);
                    pulse.calculateCurvesLength();
                    pulse.flagCurvesPositions();
                    pulse.flagLines();
                    System.out.println("Race init");
                    break;
                }
                case "gameEnd":
                    System.out.println("Race end");
                    break;
                case "gameStart":
                    System.out.println("Race start");
                    //send(new SwitchLane("Right"));
                    break;
                case "lapFinished":
                    System.out.println(msgFromServer.data);
                    break;
                case "crash":
                    Integer lastKey = pulse.tickMap.lastKey();
                    Integer pieceIndex = pulse.tickMap.get(lastKey).carPosition.piecePosition.pieceIndex;
                    System.out.println("Car crashed with speed " + pulse.tickMap.get(lastKey).speed
                            + " with radius " + pulse.race.track.pieces[pieceIndex].radius);
                    break;
                case "turboAvailable":
                    JsonObject element = gson.toJsonTree(msgFromServer.data).getAsJsonObject();
                    TurboData turboData = gson.fromJson(element, TurboData.class);
                    pulse.turboStrategy = new TurboStrategy(turboData);
                default:
                    System.out.println("MSG: " + line);
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final Integer gameTick;

    MsgWrapper(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), null);
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class CreateRace extends SendMsg {
    BotId botId;
    class BotId {
        String name;
        String key;
    }
    String trackName;
    String password;
    Integer carCount;

    CreateRace(final String name, final String key,
               final String trackName, final Integer carCount, final String password) {
        this.botId = new BotId();
        this.botId.name = name;
        this.botId.key = key;
        this.trackName = trackName;
        this.carCount = carCount;
        this.password = password;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class Turbo extends SendMsg {
    @Override
    protected Object msgData() {
        return "Pow pow pow...";
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
