package noobbot;

/**
 * @author Sergii Ostroverkhov
 * @since 19/04/2014
 */
public class Motion {
    Double a;
    Double b;
    Double c;

    Motion(double distance1, double distance2, double distance3, final double startingThrottle) {
        double s1 = distance1 / startingThrottle;
        double s2 = distance2 / startingThrottle;
        double s3 = distance3 / startingThrottle;
        a = (s3 - 2 * s2 + s1) / 2;
        b = s2 - s1 - 3 * a;
        c = s1 - a - b;
    }

    double getRatio(int time) {
        return a * time * time + b * time + c;
    }
}
