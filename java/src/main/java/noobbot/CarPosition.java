package noobbot;

/**
 * @author Sergii Ostroverkhov
 * @since 18/04/2014
 */
public class CarPosition {
    Id id;

    class Id {
        String name;
        String color;
    }

    Double angle;

    PiecePosition piecePosition;

    class PiecePosition {
        Integer pieceIndex;
        Double inPieceDistance;

        Lane lane;

        class Lane {
            Integer startLaneIndex;
            Integer endLaneIndex;
        }

        Integer lap;
    }
}
