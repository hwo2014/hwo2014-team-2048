package noobbot;

/**
 * @author Sergii Ostroverkhov
 * @since 18/04/2014
 */
public class Tick {
    Integer number;
    CarPosition carPosition;

    double throttle;
    double speed;
    double acceleration;

    double carAngleSpeed;

    public Tick(final Integer number, final CarPosition carPosition) {
        this.number = number;
        this.carPosition = carPosition;
    }

    double distance() {
        return carPosition.piecePosition.inPieceDistance;
    }
}
