package noobbot;

/**
 * @author Sergii Ostroverkhov
 * @since 21/04/2014
 */
public class TurboData {
    Double turboDurationMilliseconds;
    Integer turboDurationTicks;
    Double turboFactor;
}