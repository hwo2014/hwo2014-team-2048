package noobbot;

import com.google.gson.annotations.SerializedName;

/**
 * @author Sergii Ostroverkhov
 * @since 18/04/2014
 */
public class Race {
    Track track;

    class Track {
        String id;
        String name;

        Piece[] pieces;

        class Piece {
            Double length;
            @SerializedName("switch")
            Boolean aSwitch;

            Integer radius;
            Double angle;
            boolean turnStart;
            boolean turnMiddle;
            boolean turnEnd;
            boolean lineStart;
            boolean lineEnd;

            Double lineLength;

            public Double length(int distanceFromCenter) {
                if (radius == null) {
                    return length;
                }
                int radius = radius(distanceFromCenter);
                return Math.abs(Math.PI * radius * this.angle / 180);
            }

            public int radius(final int distanceFromCenter) {
                if (radius == null) {
                    return 0;
                }
                int radius = this.radius;
                if (angle > 0) {
                    radius = radius - distanceFromCenter;
                } else {
                    radius = radius + distanceFromCenter;
                }
                return radius;
            }

            @Override
            public String toString() {
                return "Piece{" +
                        "length=" + length +
                        ", aSwitch=" + aSwitch +
                        ", radius=" + radius +
                        ", angle=" + angle +
                        ", turnStart=" + turnStart +
                        ", turnMiddle=" + turnMiddle +
                        ", turnEnd=" + turnEnd +
                        ", lineStart=" + lineStart +
                        ", lineEnd=" + lineEnd +
                        ", lineLength=" + lineLength +
                        '}';
            }
        }

        Lane[] lanes;

        class Lane {
            Integer distanceFromCenter;
            Integer index;
        }

        StartingPoint startingPoint;

        class StartingPoint {
            Position position;

            class Position {
                Double x;
                Double y;
            }

            Double angle;
        }
    }

    Car[] cars;

    class Car {
        Id id;

        class Id {
            String name;
            String color;
        }

        Dimensions dimensions;

        class Dimensions {
            Double length;
            Double width;
            Double guideFlagPosition;
        }
    }

    RaceSession raceSession;

    class RaceSession {
        Integer laps;
        Long maxLapTimeMs;
        Boolean quickRace;
    }

    public Track.Piece getPieceAfter(final Integer pieceIndex) {
        if (pieceIndex == track.pieces.length - 1) {
            return track.pieces[0];
        } else {
            return track.pieces[pieceIndex + 1];
        }

    }

    public int getPieceIndexAfterAfter(final Integer pieceIndex) {
        if (pieceIndex == track.pieces.length - 2) {
            return 0;
        } else if (pieceIndex == track.pieces.length - 1) {
            return 1;
        } else {
            return pieceIndex + 2;
        }
    }

    public int getPieceIndexAfter(final Integer pieceIndex) {
        if (pieceIndex == track.pieces.length - 1) {
            return 0;
        } else {
            return pieceIndex + 1;
        }
    }

}
