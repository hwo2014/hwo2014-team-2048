package noobbot;

/**
 * @author Sergii Ostroverkhov
 * @since 19/04/2014
 */
public class Turn {
    Race.Track.Piece piece;
    int index;
    double distance;
    double length;

    public static Turn getNextFullTurn(final Integer pieceIndex, final Race.Track.Piece[] pieces) {
        int i = pieceIndex + 1;
        double length = 0.0;
        while (i < pieces.length) {
            if (pieces[i].radius != null) {
                Turn turn = new Turn();
                turn.index = i;
                length += pieces[i].length(-10);
                int j = i + 1;
                while (j < pieces.length && pieces[j].radius != null) {
                    length += pieces[j].length(-10);
                    j++;
                }
                turn.distance = length / 2.0;
                turn.length = length;
                break;
            }
            i++;
        }
        return null;
    }

    public static Turn getNextTurn(final Integer pieceIndex, final Race.Track.Piece[] pieces, final Double inPieceDistance, final Integer distanceFromCenter) {
        Double currentPieceLength = pieces[pieceIndex].length(distanceFromCenter);
        Turn nextTurn = getPieceWithRadius(pieceIndex + 1, pieces);
        if (nextTurn.piece == null) {
            double distance = nextTurn.distance;
            nextTurn = getPieceWithRadius(0, pieces);
            nextTurn.distance += distance;
        }
        nextTurn.distance += currentPieceLength - inPieceDistance;
        return nextTurn;
    }

    private static Turn getPieceWithRadius(final Integer fromIndex, final Race.Track.Piece[] pieces) {
        double distance = 0.0;
        Turn turn = new Turn();
        for (int i = fromIndex; i < pieces.length; i++) {
            Race.Track.Piece piece = pieces[i];
            if (piece.radius != null) {
                turn.index = i;
                turn.piece = piece;
                turn.distance = distance;
                return turn;
            } else if (piece.length != null) {
                distance += piece.length;
            }
        }
        turn.distance = distance;
        return turn;
    }

    public static boolean isCurrentPieceTurnEnd(final Race.Track.Piece piece) {
        return piece.turnEnd;
    }

    public static Turn getNextNextTurn(final Turn nextTurn, final Race.Track.Piece[] pieces) {
        Turn turn = getPieceWithRadius(nextTurn.index + 1, pieces);
        double distance = 0.0;
        while (pieces[turn.index].radius != null
                && pieces[nextTurn.index].radius != null
                && pieces[turn.index].radius.intValue() == pieces[nextTurn.index].radius ) {
            distance += turn.distance;
            turn = getPieceWithRadius(turn.index+1, pieces);
        }
        turn.distance += distance;
        return turn;
    }
}
