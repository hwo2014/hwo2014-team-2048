package noobbot;

import static java.lang.Math.sqrt;

/**
 * @author Sergii Ostroverkhov
 * @since 27/04/2014
 */
public class TurboStrategy {
    private static final int SAFE_RADIUS = 100;
    private static final double DISTANCE_PER_FACTOR = 100.0;
    private int ticks;
    private TurboData turboData;
    private boolean activated;

    public TurboStrategy(final TurboData turboData) {
        this.turboData = turboData;
    }

    public void plan(Race.Track.Piece piece, final Race.Track.Piece nextPiece) {
        Race.Track.Piece lineStartPiece;
        if (piece.lineStart) {
            lineStartPiece = piece;
        } else if (piece.turnEnd && piece.radius >= SAFE_RADIUS && nextPiece.lineStart) {
            lineStartPiece = nextPiece;
        } else {
            lineStartPiece = null;
        }
        if (lineStartPiece != null && lineStartPiece.lineLength != null
                && lineStartPiece.lineLength > turboData.turboFactor * DISTANCE_PER_FACTOR) {
            this.ticks = (int)sqrt(lineStartPiece.lineLength);
        }
    }

    public boolean ready() {
        return ticks > 0;
    }

    public boolean activated() {
        return activated;
    }

    public void activate() {
        this.activated = true;
    }

    public void tick() {
        this.ticks--;
    }

    public boolean finished() {
        return ticks <= 0;
    }
}
